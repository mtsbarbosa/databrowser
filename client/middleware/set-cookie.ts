import Cookies from 'cookie-universal';
import IProcess from "~/interfaces/IProcess";
import * as types from "~/store/mutation-types";

declare const process: IProcess;

function createCookie({req, res}) {
  return Cookies(req, res);
}

export default function (context) {
  const cookies = createCookie(context);
  const session = cookies.get('JSESSIONID');
  if(session) {
    context.store.commit(types.SET_SESSION, session);
  }
}
