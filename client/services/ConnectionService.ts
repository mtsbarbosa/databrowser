import {
  ConnectionControllerApi,
} from "~/interfaces/api";

export default class ConnectionService {

  private readonly axios;

  constructor(axios) {
    this.axios = axios;
  }

  public getConnectionController(): ConnectionControllerApi {
    return new ConnectionControllerApi(
      undefined,
      this.getApiUrl(),
      this.axios,
    );
  }

  private getApiUrl(): string {
    return this.axios.baseURI;
  }

}
