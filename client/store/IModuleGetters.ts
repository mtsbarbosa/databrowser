export interface IModuleGetters<T> {
    [key: string]: (state: T) => any;
}
