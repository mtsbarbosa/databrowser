import Vuex from "vuex";
import ConnectionModule, { IConnectionModule } from "~/store/modules/ConnectionModule";
import * as types from "~/store/mutation-types";

export interface IStoreState {
    connection: IConnectionModule;
}

const store = () => {
    return new Vuex.Store({
        modules: {
            connection: ConnectionModule,
        },
    });
};

export default store;
