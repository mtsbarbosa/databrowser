import {ColumnDetail, Connection, DatabaseConnection, InsertUpdateConnection} from "~/interfaces/api";
import ConnectionService from "~/services/ConnectionService";
import { IModuleGetters } from "~/store/IModuleGetters";
import IProcess from "~/interfaces/IProcess";
import * as types from "~/store/mutation-types";
import {isUndefined} from "util";

declare const process: IProcess;

export interface IConnectionModule {
    connections: Array<Connection>;
    databaseConnections: Array<DatabaseConnection>;
    displayedDatabaseConnection: DatabaseConnection;
    session: string;
    schemas: Array<String>;
    tables: Array<String>;
    columns: Array<ColumnDetail>;
    rows: Array<Array<String>>;
}

const state: () => IConnectionModule = () => {
    return {
        connections: [],
        databaseConnections: [],
        displayedDatabaseConnection: {},
        session: "",
        schemas: [],
        tables: [],
        columns: [],
        rows: [],
    };
};

const getters: IModuleGetters<IConnectionModule> = {
    connections: (state) => {
      return state.connections;
    },
    databaseConnections: (state) => {
      return state.databaseConnections;
    },
    displayedDatabaseConnection: (state) => {
      return state.displayedDatabaseConnection;
    },
    session: (state) => {
      return state.session;
    },
    schemas: (state) => {
      return state.schemas;
    },
    tables: (state) => {
      return state.tables;
    },
    columns: (state) => {
      return state.columns;
    },
    rows: (state) => {
        return state.rows;
    }
};

const mutations = {
    [types.PATCH_DATABASE_CONNECTION](state: IConnectionModule, databaseConnection: Partial<DatabaseConnection>) {
      state.databaseConnections.push(databaseConnection);
    },
    [types.PATCH_CONNECTION](state: IConnectionModule, connection: Partial<Connection>) {
      state.connections.push(connection);
    },
    [types.PATCH_CONNECTIONS](state: IConnectionModule, connections: Array<Connection>) {
      state.connections = connections;
    },
    [types.SET_DISPLAYED_DATABASE_CONNECTION](state: IConnectionModule, displayedDatabaseConnection: DatabaseConnection) {
      state.displayedDatabaseConnection = displayedDatabaseConnection;
    },
    [types.SET_SESSION](state: IConnectionModule, session: string) {
      state.session = session;
    },
    [types.PATCH_SCHEMAS](state: IConnectionModule, schemas: Array<String>) {
      state.schemas = schemas;
    },
    [types.PATCH_TABLES](state: IConnectionModule, tables: Array<String>) {
      state.tables = tables;
    },
    [types.PATCH_COLUMNS](state: IConnectionModule, columns: Array<ColumnDetail>) {
      state.columns = columns;
    },
    [types.PATCH_ROWS](state: IConnectionModule, rows: Array<Array<String>>) {
      state.rows = rows;
    },
};

const actions = {
    async [types.FETCH_DATABASE_CONNECTION]({ commit, dispatch }, id?: number) {
      try {
        if(!isUndefined(id)) {
          const {data} = await new ConnectionService(this.$axios).getConnectionController().createConnection(id);
          commit(types.PATCH_DATABASE_CONNECTION, data);
          dispatch(types.DISPLAY_DATABASE_CONNECTION, data);
        }
      } catch (error) {
        return false;
      }
    },
    async [types.FETCH_CONNECTION]({ commit, dispatch }, {connection, id}) {
      try {
        let data;
        if(connection) {
          if(id){
            data = await new ConnectionService(this.$axios).getConnectionController().putConnections(id, connection);
          } else {
            data = await new ConnectionService(this.$axios).getConnectionController().postConnections(connection);
          }
          dispatch(types.FETCH_CONNECTIONS, {});
        }

      } catch (error) {
        return false;
      }
    },
    async [types.DELETE_CONNECTION]({ commit, dispatch }, id? : number) {
      try {
        let data;
        if(id){
          data = await new ConnectionService(this.$axios).getConnectionController().deleteConnections(id);
        }
        dispatch(types.FETCH_CONNECTIONS, {});

      } catch (error) {
        return false;
      }
    },
    async [types.FETCH_CONNECTIONS]({ commit, dispatch }, {id, name}) {
      try {
        let data;
        if(id) {
          data = await new ConnectionService(this.$axios).getConnectionController().getConnectionsById(id);
        } else if(name) {
          data = [await new ConnectionService(this.$axios).getConnectionController().getConnections(name)];
        } else {
          data = await new ConnectionService(this.$axios).getConnectionController().getConnections();
        }
        if(!isUndefined(data) && data) {
          commit(types.PATCH_CONNECTIONS, data.data);
        }
      } catch (error) {
        return false;
      }
    },
    async [types.FETCH_SCHEMAS]({ commit, dispatch }, connection_id: number) {
      try {
          const {data} = await new ConnectionService(this.$axios).getConnectionController().getSchemasFromConnection(connection_id);
        if(!isUndefined(data) && data) {
          commit(types.PATCH_SCHEMAS, data);
        }
      } catch (error) {
        return false;
      }
    },
    async [types.FETCH_TABLES]({ commit, dispatch }, connection_id: number) {
      try {
        const {data} = await new ConnectionService(this.$axios).getConnectionController().getTablesFromConnection(connection_id);
        if(!isUndefined(data) && data) {
          commit(types.PATCH_TABLES, data);
        }
      } catch (error) {
        return false;
      }
    },
    async [types.FETCH_COLUMNS]({ commit, dispatch }, {connection_id, table_name}) {
      try {
        const {data} = await new ConnectionService(this.$axios).getConnectionController().getColumnsFromTable(connection_id,table_name);
        if(!isUndefined(data) && data) {
          commit(types.PATCH_COLUMNS, data);
        }
      } catch (error) {
        return false;
      }
    },
    async [types.FETCH_ROWS]({ commit, dispatch }, {connection_id, table_name}) {
      try {
        const {data} = await new ConnectionService(this.$axios).getConnectionController().getRowsFromTable(connection_id,table_name);
        if(!isUndefined(data) && data) {
          commit(types.PATCH_ROWS, data);
        }
      } catch (error) {
        return false;
      }
    },
    async [types.DISPLAY_DATABASE_CONNECTION]({ commit, dispatch }, databaseConnection: DatabaseConnection) {
      try {
        commit(types.SET_DISPLAYED_DATABASE_CONNECTION, databaseConnection);
        commit(types.PATCH_SCHEMAS, []);
        commit(types.PATCH_TABLES, []);
        commit(types.PATCH_COLUMNS, []);
        commit(types.PATCH_ROWS, []);
        const path = "/databaseconnection";
        if (process.server) {
          this.app.context.redirect(path);
        } else {
            if (this.app.router.currentRoute.path == path) {
              this.app.context.redirect(path);
            } else {
              this.app.router.push(path);
            }
        }
      } catch (error) {
        return false;
      }
    },
};

const ConnectionModule = {
    state,
    getters,
    mutations,
    actions,
};

export default ConnectionModule;
