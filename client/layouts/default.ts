import Component from "nuxt-class-component";
import Vue from "vue";

@Component
export default class DefaultLayout extends Vue{
  public clipped: boolean = false;
  public drawer: boolean = false;
  public fixed: boolean = false;
  public items: Array<object> = [
    {
      icon: 'mdi-list',
      title: 'List Connections',
      to: '/connections'
    },
    {
      icon: 'mdi-swap_horizontal_circle',
      title: 'Database Connection',
      to: '/databaseconnection'
    },
  ];
  public miniVariant: boolean = false;
  public right: boolean = true;
  public rightDrawer: boolean = false;
  public title: string = "Data Browser";
}
