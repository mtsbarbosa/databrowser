import Component from "nuxt-class-component";
import { Action, Getter, Mutation } from "vuex-class";
import * as types from "~/store/mutation-types";
import Vue from "vue";
import Utils from "~/utils/Utils";
import {ColumnDetail, Connection, DatabaseConnection, InsertUpdateConnection} from "~/interfaces/api";

@Component
export default class DatabaseConnectionPageImpl extends Vue {
  @Getter public displayedDatabaseConnection : DatabaseConnection;
  @Getter public schemas : Array<String>;
  @Getter public tables : Array<String>;
  @Getter public columns : Array<ColumnDetail>;
  @Getter public rows : Array<Array<String>>;

  public selectedTable: string = "";
  public panel : Array<number> = [];

  public async loadSchemas(){
    this.$store.dispatch(types.FETCH_SCHEMAS, this.displayedDatabaseConnection.detailsId);
  }

  public async loadTables(){
    this.$store.dispatch(types.FETCH_TABLES, this.displayedDatabaseConnection.detailsId);
  }

  public async selectTable(tableName: string){
    this.selectedTable = tableName;
    await this.loadColumns();
  }

  public async loadColumns(){
    this.$store.dispatch(types.FETCH_COLUMNS, { connection_id: this.displayedDatabaseConnection.detailsId, table_name: this.selectedTable });
  }

  public async loadRows(){
    this.panel = [1,2];
    this.$store.dispatch(types.FETCH_ROWS, { connection_id: this.displayedDatabaseConnection.detailsId, table_name: this.selectedTable });
    const container = this.$el.querySelector("#datapanel");
    if(container) {
      container.scrollTop = container.scrollHeight;
    }
  }
}
