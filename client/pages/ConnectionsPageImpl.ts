import Component from "nuxt-class-component";
import { Action, Getter, Mutation } from "vuex-class";
import * as types from "~/store/mutation-types";
import Vue from "vue";
import Utils from "~/utils/Utils";
import {Connection, DatabaseConnection, InsertUpdateConnection} from "~/interfaces/api";

@Component
export default class ConnectionsPageImpl extends Vue {
    @Action(types.FETCH_DATABASE_CONNECTION) public connect;
    @Action(types.FETCH_CONNECTION) public saveConnection;
    @Action(types.DELETE_CONNECTION) public deleteConnection;
    @Getter public connections : Array<Connection>;
    @Getter public databaseConnections : Array<DatabaseConnection>;

    public toSaveConnection: InsertUpdateConnection = {};
    public dialog: boolean = false;
    public editedIndex: number = -1;

    public async fetch({ store, app: { context: { req } } }) {
      await store.dispatch(types.FETCH_CONNECTIONS, {});
    }

    public connectItem(connection){
      this.connect(connection.id);
    }

    public editItem(connection){
      this.editedIndex = connection.id;
      this.toSaveConnection = Object.assign({}, connection);
      this.dialog = true;
    }

    public insertItem(){
      this.editedIndex = -1;
      this.dialog = true;
    }

    public deleteItem(connection){
      const index = connection.id;
      if(confirm('Are you sure you want to delete this item?')) {
        this.deleteConnection(index);
      }
    }

    public close(){
      this.dialog = false;
      this.toSaveConnection = {};
      this.editedIndex = -1;
    }

    public save() {
      if (this.editedIndex > -1) {
        this.saveConnection({connection: this.toSaveConnection, id: this.editedIndex});
      } else {
        this.saveConnection({connection: this.toSaveConnection});
      }
      this.close();
    }
}
