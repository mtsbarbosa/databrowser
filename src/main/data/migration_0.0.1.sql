USE databrowser;
CREATE TABLE `connection` (
	`id` BIGINT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(30) NOT NULL,
	`hostname` VARCHAR(30) NOT NULL,
	`port` VARCHAR(5) NOT NULL,
	`database_name` VARCHAR(12) NOT NULL,
	`username` VARCHAR(20) NOT NULL,
	`password` VARCHAR(16) NOT NULL,
	UNIQUE KEY `idx_conn_name` (`name`) USING HASH,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO CONNECTION (name,hostname,port,database_name,username,password)
VALUES
(
  'database1',
  '127.0.0.1',
  '3306',
  'mysql',
  'data_admin',
  'secret'
);