package com.mcarvalho.databrowser.repository;

import com.mcarvalho.databrowser.lang.DriverNotFoundException;
import com.mcarvalho.databrowser.model.Connection;
import com.mcarvalho.databrowser.model.dto.ColumnDetailDTO;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Repository
public class DatasourceManager {

	public static enum DatabaseNameEnum{
		mysql(
				"com.mysql.cj.jdbc.Driver",
				"SELECT schema_name as schema_name "
						+ "FROM information_schema.schemata "
						+ "ORDER BY schema_name",
				"SELECT table_name "
						+ "FROM information_schema.tables "
						+ "ORDER BY table_name ASC",
				"SELECT column_name, data_type "
						+ "FROM information_schema.columns "
						+ "where table_name = ? "
						+ "ORDER BY table_name,ordinal_position"
		),
		oracle(
				"oracle.jdbc.driver.OracleDriver",
				"SELECT username as schema_name "
						+ " FROM sys.all_users "
						+ " ORDER BY username",
				"SELECT table_name "
						+ "FROM user_tables "
						+ "ORDER BY table_name",
				"SELECT column_name, data_type "
						+ "FROM user_tab_cols "
						+ "WHERE table_name = ?"
		),
		postgresql(
				"org.postgresql.Driver",
				"",
				"",
				""
		);

		DatabaseNameEnum(String driverClass, String queryListSchemes, String queryListTables, String queryListColumns) {
			this.driverClass = driverClass;
			this.queryListSchemes = queryListSchemes;
			this.queryListTables = queryListTables;
			this.queryListColumns = queryListColumns;
		}

		private String driverClass;
		private String queryListSchemes;
		private String queryListTables;
		private String queryListColumns;

		public String getDriverClass() {
			return driverClass;
		}

		public String getQueryListSchemes() {
			return queryListSchemes;
		}

		public String getQueryListTables() {
			return queryListTables;
		}

		public String getQueryListColumns() {
			return queryListColumns;
		}
	}

	public java.sql.Connection connect(Connection connection) throws DriverNotFoundException, SQLException {
		DatabaseNameEnum databaseName = getDatabaseNameEnum(connection);
		DataSource dataSource =
				DataSourceBuilder
						.create()
						.driverClassName(databaseName.getDriverClass())
						.url(databaseName.equals(DatabaseNameEnum.oracle)?
										String.format("jdbc:%s:thin:%s/%s@%s:%s:xe",databaseName.name(),connection.getUsername(),connection.getPassword(),connection.getHostname(),connection.getPort()):
										String.format("jdbc:%s://%s:%s/databrowser",databaseName.name(),connection.getHostname(),connection.getPort()))
						.username(connection.getUsername())
						.password(connection.getPassword())
						.build();
		return dataSource.getConnection();
	}

	public List<String> findAllSchemas(java.sql.Connection databaseConnection, Connection connection) throws DriverNotFoundException, SQLException{
		DatabaseNameEnum databaseName = getDatabaseNameEnum(connection);
		if(!databaseConnection.isClosed()){
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(databaseName.getQueryListSchemes());
			ArrayList<String> schemas = new ArrayList<>();
			while (rs.next()) {
				schemas.add(rs.getString("schema_name"));
			}
			return schemas;
		}
		return null;
	}

	public List<String> findAllTables(java.sql.Connection databaseConnection, Connection connection) throws DriverNotFoundException, SQLException{
		DatabaseNameEnum databaseName = getDatabaseNameEnum(connection);
		if(!databaseConnection.isClosed()){
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery(databaseName.getQueryListTables());
			ArrayList<String> tables = new ArrayList<>();
			while (rs.next()) {
				tables.add(rs.getString("table_name"));
			}
			return tables;
		}
		return null;
	}

	public List<ColumnDetailDTO> findAllColumnsByTable(java.sql.Connection databaseConnection, Connection connection, String tableName) throws DriverNotFoundException, SQLException{
		DatabaseNameEnum databaseName = getDatabaseNameEnum(connection);
		if(!databaseConnection.isClosed()){
			PreparedStatement stmt = databaseConnection.prepareStatement(databaseName.getQueryListColumns());
			stmt.setString(1, tableName);
			ResultSet rs = stmt.executeQuery();
			ArrayList<ColumnDetailDTO> columns = new ArrayList<>();
			while (rs.next()) {
				ColumnDetailDTO columnDetail = new ColumnDetailDTO(
						rs.getString("column_name"),
						rs.getString("data_type")
				);
				columns.add(columnDetail);
			}
			return columns;
		}
		return null;
	}

	public List<List<String>> previewData(java.sql.Connection databaseConnection, Connection connection, String tableName) throws DriverNotFoundException, SQLException{
		DatabaseNameEnum databaseName = getDatabaseNameEnum(connection);
		if(!databaseConnection.isClosed()){
			Statement stmt = databaseConnection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM " + tableName);
			ArrayList<List<String>> rows = new ArrayList<>();
			int columnCount = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				ArrayList<String> values = new ArrayList<>();
				for (int column = 1; column <= columnCount; column++) {
					final Object value = rs.getObject(column);
					values.add(String.valueOf(value));
				}
				rows.add(values);
			}
			return rows;
		}
		return null;
	}

	private DatabaseNameEnum getDatabaseNameEnum(Connection connection) throws DriverNotFoundException {
		DatabaseNameEnum databaseName = DatabaseNameEnum.valueOf(connection.getDatabaseName());
		if(databaseName == null){
			throw new DriverNotFoundException();
		}
		return databaseName;
	}
}
