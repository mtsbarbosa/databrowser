package com.mcarvalho.databrowser.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class TableMetadataRepository
{
	@Autowired
	private EntityManager entityManager;

	public List<String> findAllTables() {
		return entityManager.createNativeQuery("SELECT table_name FROM information_schema.tables ORDER BY table_name ASC")
				.getResultList();
	}
}
