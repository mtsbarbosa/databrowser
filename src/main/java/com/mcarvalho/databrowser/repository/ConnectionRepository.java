package com.mcarvalho.databrowser.repository;

import com.mcarvalho.databrowser.model.Connection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface ConnectionRepository extends CrudRepository<Connection, Long> {
	public Set<Connection> findAllBy();
	public Connection findConnectionByName(String name);
	public Connection findConnectionById(Long id);
}
