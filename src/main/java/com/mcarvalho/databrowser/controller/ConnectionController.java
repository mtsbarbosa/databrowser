package com.mcarvalho.databrowser.controller;

import com.mcarvalho.databrowser.lang.NotFoundException;
import com.mcarvalho.databrowser.model.Connection;
import com.mcarvalho.databrowser.model.dto.ColumnDetailDTO;
import com.mcarvalho.databrowser.model.dto.ConnectionAttributeDTO;
import com.mcarvalho.databrowser.service.ConnectionService;
import com.mcarvalho.databrowser.util.ConnectionAttribute;
import com.mcarvalho.databrowser.util.SessionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class ConnectionController {
	@Autowired
	private ConnectionService connectionService;

	@GetMapping("/connections/{id}")
	public Connection getConnection(@PathVariable("id") Long id) {
		return connectionService.findById(id);
	}

	@GetMapping("/connections")
	public ResponseEntity<Set<Connection>> getConnectionByName(@RequestParam(value = "name", required = false) String name) {
		if(StringUtils.isEmpty(name))
			return ResponseEntity.ok(connectionService.findAll());
		try {
			return ResponseEntity.ok(Stream.of(connectionService.findByName(name)).collect(Collectors.toSet()));
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("/connections")
	public ResponseEntity<Connection> postConnection(@Valid @RequestBody Connection connection) {
		connection = connectionService.save(connection);
		return ResponseEntity.ok(connection);
	}

	@PutMapping("/connections/{id}")
	public ResponseEntity<Connection> putConnection(@PathVariable("id") Long id, @Valid @RequestBody Connection connection) {
		Connection savedConnection;
		try{
			savedConnection = connectionService.update(id, connection);
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(savedConnection);
	}

	@DeleteMapping("/connections/{id}")
	public void deleteConnection(@PathVariable("id") Long id) {
		connectionService.delete(id);
	}

	@PostMapping("/connections/{id}/connect")
	public ResponseEntity connect(@PathVariable("id") Long id) {
		try{
			HttpSession session = SessionUtil.getSession(true);
			ConnectionAttribute connectionAttribute = initializeConnectionAttribute(id, session);
			ConnectionAttributeDTO connectionAttributeDTO = new ConnectionAttributeDTO(connectionAttribute.getId(), id);
			return ResponseEntity.ok().body(connectionAttributeDTO);
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}catch (SQLException e){
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Connection unauthorized");
		}
	}

	private ConnectionAttribute initializeConnectionAttribute(Long id, HttpSession session) throws NotFoundException, SQLException {
		java.sql.Connection sqlConnection = connectionService.connect(id);
		ConnectionAttribute connectionAttribute = new ConnectionAttribute(sqlConnection, id);
		SessionUtil.addItemToAttributeList(session,SessionUtil.ATTR_CONNECTIONS,connectionAttribute);
		return connectionAttribute;
	}

	@GetMapping("/connections/{id}/schemas")
	public ResponseEntity schemas(@PathVariable("id") Long id) {
		try{
			HttpSession session = SessionUtil.getSession(true);
			ConnectionAttribute connectionAttribute = SessionUtil.getConnectionFromAttributeList(session,id);
			if(connectionAttribute == null){
				connectionAttribute = initializeConnectionAttribute(id, session);
			}
			List<String> schemas = connectionService.findAllSchemas(connectionAttribute.getConnection(),id);
			return ResponseEntity.ok().body(schemas);
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}catch (SQLException e){
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Connection unauthorized");
		}
	}

	@GetMapping("/connections/{id}/tables")
	public ResponseEntity tables(@PathVariable("id") Long id) {
		try{
			HttpSession session = SessionUtil.getSession(true);
			ConnectionAttribute connectionAttribute = SessionUtil.getConnectionFromAttributeList(session,id);
			if(connectionAttribute == null){
				connectionAttribute = initializeConnectionAttribute(id, session);
			}
			List<String> tables = connectionService.findAllTables(connectionAttribute.getConnection(),id);
			return ResponseEntity.ok().body(tables);
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}catch (SQLException e){
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Connection unauthorized");
		}
	}

	@GetMapping("/connections/{id}/tables/columns")
	public ResponseEntity columns(@PathVariable("id") Long id, @RequestParam(value = "tableName", required = false) String tableName) {
		try{
			HttpSession session = SessionUtil.getSession(true);
			ConnectionAttribute connectionAttribute = SessionUtil.getConnectionFromAttributeList(session,id);
			if(connectionAttribute == null){
				connectionAttribute = initializeConnectionAttribute(id, session);
			}
			List<ColumnDetailDTO> columns = connectionService.findColumnsByTable(connectionAttribute.getConnection(),id, tableName);
			return ResponseEntity.ok().body(columns);
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}catch (SQLException e){
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Connection unauthorized");
		}
	}

	@GetMapping("/connections/{id}/tables/preview")
	public ResponseEntity preview(@PathVariable("id") Long id, @RequestParam(value = "tableName", required = false) String tableName) {
		try{
			HttpSession session = SessionUtil.getSession(true);
			ConnectionAttribute connectionAttribute = SessionUtil.getConnectionFromAttributeList(session,id);
			if(connectionAttribute == null){
				connectionAttribute = initializeConnectionAttribute(id, session);
			}
			List<String> tables = connectionService.findAllTables(connectionAttribute.getConnection(),id);
			if(!tables.contains(tableName)){
				return ResponseEntity.notFound().build();
			}
			List<List<String>> rows = connectionService.previewData(connectionAttribute.getConnection(),id, tableName);
			return ResponseEntity.ok().body(rows);
		}catch (NotFoundException e){
			return ResponseEntity.notFound().build();
		}catch (SQLException e){
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Connection unauthorized");
		}
	}
}
