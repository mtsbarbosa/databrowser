package com.mcarvalho.databrowser.controller;

import com.mcarvalho.databrowser.repository.TableMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class TableController {
	@Autowired
	TableMetadataRepository tableMetadataRepository;

	@GetMapping("/tables")
	public List<String> getAllTables() {
		return tableMetadataRepository.findAllTables();
	}
}
