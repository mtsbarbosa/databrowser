package com.mcarvalho.databrowser.service;

import com.mcarvalho.databrowser.lang.DriverNotFoundException;
import com.mcarvalho.databrowser.lang.NotFoundException;
import com.mcarvalho.databrowser.model.Connection;
import com.mcarvalho.databrowser.model.dto.ColumnDetailDTO;
import com.mcarvalho.databrowser.repository.ConnectionRepository;
import com.mcarvalho.databrowser.repository.DatasourceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class ConnectionService {
	@Autowired
	private ConnectionRepository connectionRepository;
	@Autowired
	private DatasourceManager datasourceManager;

	public Set<Connection> findAll(){
		return connectionRepository.findAllBy();
	}

	public Connection findByName(String name) throws NotFoundException{
		Connection connection = connectionRepository.findConnectionByName(name);
		if(connection == null){
			throw new NotFoundException();
		}
		return connection;
	}

	public Connection findById(Long id){
		return connectionRepository.findConnectionById(id);
	}

	@Transactional(rollbackFor = Exception.class)
	public Connection update(Long id, Connection connection) throws NotFoundException {
		Optional<Connection> optConnection = connectionRepository.findById(id);
		if(!optConnection.isPresent()){
			throw new NotFoundException();
		}
		optConnection.ifPresent(con -> {
			con.setDatabaseName(connection.getDatabaseName());
			con.setHostname(connection.getHostname());
			con.setName(connection.getName());
			con.setPassword(connection.getPassword());
			con.setPort(connection.getPort());
			con.setName(connection.getName());
			connectionRepository.save(con);
		});
		return optConnection.get();
	}

	@Transactional(rollbackFor = Exception.class)
	public Connection save(Connection connection){
		return connectionRepository.save(connection);
	}

	@Transactional(rollbackFor = Exception.class)
	public void delete(Long id){
		connectionRepository.deleteById(id);
	}

	public java.sql.Connection connect(Long id) throws NotFoundException, SQLException {
		Connection connection = connectionRepository.findConnectionById(id);
		if(connection == null){
			throw new NotFoundException();
		}
		return datasourceManager.connect(connection);
	}

	public List<String> findAllSchemas(java.sql.Connection databaseConnection, Long id) throws NotFoundException, SQLException {
		Connection connection = connectionRepository.findConnectionById(id);
		if(connection == null){
			throw new NotFoundException();
		}
		List<String> schemas = datasourceManager.findAllSchemas(databaseConnection, connection);
		if(schemas == null){
			throw new NotFoundException();
		}
		return schemas;
	}

	public List<String> findAllTables(java.sql.Connection databaseConnection, Long id) throws NotFoundException, SQLException {
		Connection connection = connectionRepository.findConnectionById(id);
		if(connection == null){
			throw new NotFoundException();
		}
		List<String> tables = datasourceManager.findAllTables(databaseConnection, connection);
		if(tables == null){
			throw new NotFoundException();
		}
		return tables;
	}

	public List<ColumnDetailDTO> findColumnsByTable(java.sql.Connection databaseConnection, Long id, String tableName) throws NotFoundException, SQLException {
		Connection connection = connectionRepository.findConnectionById(id);
		if(connection == null){
			throw new NotFoundException();
		}
		List<ColumnDetailDTO> columns = datasourceManager.findAllColumnsByTable(databaseConnection, connection, tableName);
		if(columns == null){
			throw new NotFoundException();
		}
		return columns;
	}

	public List<List<String>> previewData(java.sql.Connection databaseConnection, Long id, String tableName) throws NotFoundException, SQLException {
		Connection connection = connectionRepository.findConnectionById(id);
		if(connection == null){
			throw new NotFoundException();
		}
		List<List<String>> rows = datasourceManager.previewData(databaseConnection, connection, tableName);
		if(rows == null){
			throw new NotFoundException();
		}
		return rows;
	}
}
