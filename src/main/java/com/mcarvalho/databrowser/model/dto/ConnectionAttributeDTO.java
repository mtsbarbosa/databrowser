package com.mcarvalho.databrowser.model.dto;

import java.io.Serializable;
import java.util.UUID;

public class ConnectionAttributeDTO implements Serializable {
	private UUID id;
	private Long detailsId;

	public ConnectionAttributeDTO(UUID id, Long detailsId) {
		this.id = id;
		this.detailsId = detailsId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Long getDetailsId() {
		return detailsId;
	}

	public void setDetailsId(Long detailsId) {
		this.detailsId = detailsId;
	}
}
