package com.mcarvalho.databrowser.util;

import org.springframework.util.CollectionUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public class SessionUtil {
	public static final String ATTR_CONNECTIONS = "connections";

	public static HttpSession getSession(){
		return getSession(false);
	}

	public static HttpSession getSession(boolean createSession){
		ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(createSession);
	}

	public static boolean hasSession(){
		return getSession() != null;
	}

	public static <T extends Object> void addItemToAttributeList(HttpSession session, String attributeKey, T item){
		List<T> items = (List<T>) session.getAttribute(attributeKey);
		if(CollectionUtils.isEmpty(items)){
			items = new ArrayList<>();
		}
		Optional<T> itemFound = items.stream().filter(Predicate.isEqual(item)).findFirst();
		if(!itemFound.isPresent()){
			items.add(item);
		}
		session.setAttribute(attributeKey,items);
	}

	public static ConnectionAttribute getConnectionFromAttributeList(HttpSession session, Long id){
		List<ConnectionAttribute> items = (List<ConnectionAttribute>) session.getAttribute(ATTR_CONNECTIONS);
		if(!CollectionUtils.isEmpty(items)){
			Optional<ConnectionAttribute> itemFound = items.stream().filter(connectionAttribute -> connectionAttribute.getDetailsId().equals(id)).findFirst();
			if(itemFound.isPresent()){
				return itemFound.get();
			}
		}
		return null;
	}
}

