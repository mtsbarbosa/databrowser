package com.mcarvalho.databrowser.util;

import java.sql.Connection;
import java.util.UUID;

public class ConnectionAttribute {
	private UUID id;
	private Connection connection;
	private Long detailsId;

	public ConnectionAttribute(Connection connection, Long detailsId) {
		this.id = UUID.randomUUID();
		this.connection = connection;
		this.detailsId = detailsId;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public Long getDetailsId() {
		return detailsId;
	}

	public void setDetailsId(Long detailsId) {
		this.detailsId = detailsId;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		return this.id.equals(((ConnectionAttribute)obj).getId());
	}
}
