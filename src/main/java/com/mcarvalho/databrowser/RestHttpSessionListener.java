package com.mcarvalho.databrowser;

import com.mcarvalho.databrowser.util.ConnectionAttribute;
import com.mcarvalho.databrowser.util.SessionUtil;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebListener
public class RestHttpSessionListener implements HttpSessionListener {
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		if(se.getSession() != null){
			List<ConnectionAttribute> connections = (List<ConnectionAttribute>) se.getSession().getAttribute(SessionUtil.ATTR_CONNECTIONS);
			if(connections != null){
				try {
					for(ConnectionAttribute connectionAttribute : connections){
						Connection connection = connectionAttribute.getConnection();
						if(connection != null && !connection.isClosed()){
							connection.close();
						}
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
