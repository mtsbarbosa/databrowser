package com.mcarvalho.databrowser.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.HttpSession;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class SessionUtilTest {

	@Test
	public void whenSessionIsEmpty_thenListIsCreatedAndHasOneItemAdded(){
		HttpSession session = new MockHttpSession();
		ConnectionAttribute connectionAttribute = new ConnectionAttribute(null,1l);
		SessionUtil.addItemToAttributeList(session,SessionUtil.ATTR_CONNECTIONS,connectionAttribute);
		assertEquals(((List)session.getAttribute(SessionUtil.ATTR_CONNECTIONS)).size(),1);
	}

	@Test
	public void whenSessionHasSameConnection_thenListDoesNotIncrease(){
		HttpSession session = new MockHttpSession();
		((MockHttpSession) session).clearAttributes();
		ConnectionAttribute connectionAttribute = new ConnectionAttribute(null,1l);
		SessionUtil.addItemToAttributeList(session,SessionUtil.ATTR_CONNECTIONS,connectionAttribute);
		assertEquals(((List)session.getAttribute(SessionUtil.ATTR_CONNECTIONS)).size(),1);
		SessionUtil.addItemToAttributeList(session,SessionUtil.ATTR_CONNECTIONS,connectionAttribute);
		assertEquals(((List)session.getAttribute(SessionUtil.ATTR_CONNECTIONS)).size(),1);
	}

	@Test
	public void whenSessionHasDifferentConnection_thenListSizeIncreases(){
		HttpSession session = new MockHttpSession();
		((MockHttpSession) session).clearAttributes();
		ConnectionAttribute connectionAttribute = new ConnectionAttribute(null,1l);
		SessionUtil.addItemToAttributeList(session,SessionUtil.ATTR_CONNECTIONS,connectionAttribute);
		assertEquals(((List)session.getAttribute(SessionUtil.ATTR_CONNECTIONS)).size(),1);
		connectionAttribute = new ConnectionAttribute(null,1l);
		SessionUtil.addItemToAttributeList(session,SessionUtil.ATTR_CONNECTIONS,connectionAttribute);
		assertEquals(((List)session.getAttribute(SessionUtil.ATTR_CONNECTIONS)).size(),2);
	}
}
