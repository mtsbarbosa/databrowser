FROM tomcat:latest
ARG rel_n=0.0.1-SNAPSHOT
COPY ./ROOT.xml ./conf/Catalina/localhost/ROOT.xml
COPY ./target/databrowser-${rel_n}.war ./webapps/databrowser.war
CMD ["catalina.sh", "run"]

EXPOSE 8080
