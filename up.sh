#!/usr/bin/env bash
echo "Deploying Server"
mvn clean
mvn compile war:war
sudo docker-compose up -d
echo "Server is deployed"
echo "Deploying Client"
cd client
echo "Building Docker"
sudo docker-compose build
sudo docker-compose up -d client
echo "Client is deployed"
