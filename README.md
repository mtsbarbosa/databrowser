# Data Browser

Hi! This is the **Data Browser** web app powered by Mateus Carvalho
The purpose of this web app is to maintain own connections, connect with them and browse their data
Currently it has support only to **Oracle** and **MySql** databases, it is going to be extended


# Pre-requisites

It is designed to run easier on linux (using bash)
If you dont have linux open up.sh file and do same steps in your SO

Some pre-requisites:

 - Maven
 - Java
 - Node.js with npm
 - Docker with docker compose
 - ports 8080, 3306 and 3000 free for use (unless you change configuration files)

## Initial setup

After cloning the repository execute:

    $ sudo docker-compose -d up databrowser_mysql

After mysql is up, open src/main/data/migration_0.0.1.sql and execute its content in mysql console
these are default configuration for it:

    jdbc:mysql://127.0.0.1:3306/databrowser
    user: data_admin
    password: secret



## Deploying all in one

If you have linux simply execute

    sudo sh up.sh
    

## API documentation
It can be found at /client/static/openapi/index.html



## Addresses
 - Database: jdbc:mysql://127.0.0.1:3306/databrowser
 - API Server: http://127.0.0.1:8080
 - Client ui: http://127.0.0.1:3000